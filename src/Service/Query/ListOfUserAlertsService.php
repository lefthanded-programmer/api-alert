<?php

declare(strict_types=1);

namespace App\Service\Query;

use Exception;
use App\{Message\Query\UserAlerts, Repository\AlertTypeRepository, Service\ServiceResponse};
use Symfony\Component\{HttpFoundation\JsonResponse,
    HttpFoundation\Response,
    Messenger\MessageBusInterface,
    Messenger\Stamp\HandledStamp,
    Uid\Uuid};
use InvalidArgumentException;

class ListOfUserAlertsService extends ServiceResponse
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private AlertTypeRepository $alertKindRepository
    ) { }

    public function get(string $userId, int $limit, int $offset): JsonResponse
    {
        try {
            $handledStamp = $this->messageBus
                ->dispatch(new UserAlerts(Uuid::fromString($userId), $limit, $offset))
                ->last(HandledStamp::class);

            $response = $this->prepareResponse(
                'Executed successfully.',
                Response::HTTP_OK,
                $handledStamp->getResult()
            );
        } catch (InvalidArgumentException $exception) {
            $response = $this->prepareResponse((string) $exception, Response::HTTP_NOT_ACCEPTABLE);
        } catch (Exception $exception) {
            $response = $this->prepareResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }
}
