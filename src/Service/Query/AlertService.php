<?php

declare(strict_types=1);

namespace App\Service\Query;

use Exception;
use App\{Message\Query\Alert, Service\ServiceResponse};
use Symfony\{Component\HttpFoundation\JsonResponse,
    Component\HttpFoundation\Response,
    Component\Messenger\MessageBusInterface,
    Component\Messenger\Stamp\HandledStamp,
    Component\Uid\Uuid};
use InvalidArgumentException;

class AlertService extends ServiceResponse
{
    public function __construct(
        private MessageBusInterface $messageBus,
    ) { }

    public function get(string $alertId): JsonResponse
    {
        try {
            $handledStamp = $this->messageBus
                ->dispatch(new Alert(Uuid::fromString($alertId)))
                ->last(HandledStamp::class);

            $response = $this->prepareResponse(
                'Executed successfully.',
                Response::HTTP_OK,
                $handledStamp->getResult()->toArray()
            );
        } catch (InvalidArgumentException $exception) {
            $response = $this->prepareResponse((string) $exception, Response::HTTP_NOT_ACCEPTABLE);
        } catch (Exception $exception) {
            $response = $this->prepareResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }
}
