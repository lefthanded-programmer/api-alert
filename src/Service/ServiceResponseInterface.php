<?php

declare(strict_types=1);

namespace App\Service;

interface ServiceResponseInterface
{
    public function prepareResponse($message, $code, $content = []): array;
}