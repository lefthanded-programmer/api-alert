<?php

declare(strict_types=1);

namespace App\Service\Command;

use Exception;
use InvalidArgumentException;
use App\{Message\Command\UpdateAlert,
    Message\Query\Alert as AlertQuery,
    Message\Query\AlertType,
    Repository\AlertTypeRepository,
    Service\ServiceResponse};
use Symfony\Component\{HttpFoundation\JsonResponse,
    HttpFoundation\Request,
    HttpFoundation\Response,
    Messenger\MessageBusInterface,
    Messenger\Stamp\HandledStamp,
    Uid\Uuid,
    Validator\Validator\ValidatorInterface};

class UpdateAlertService extends ServiceResponse
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private AlertTypeRepository $alertTypeRepository,
        private ValidatorInterface $validator
    ) { }

    public function update(string $alertId, Request $request): JsonResponse
    {
        try {
            $UuidAlertId = Uuid::fromString($alertId);

            $alert = $this->messageBus
                ->dispatch(new AlertQuery($UuidAlertId))
                ->last(HandledStamp::class)
                ->getResult();

            $requestParams = $this->getRequestParams($request);

            if (isset($requestParams['type'])) $alert->setType(
                $this->messageBus
                    ->dispatch(new AlertType((int) $requestParams['type']))
                    ->last(HandledStamp::class)
                    ->getResult(),
            );
            if (isset($requestParams['value'])) $alert->setValue((int) $requestParams['value']);

            $errors = $this->validator->validate($alert);

            if (count($errors) > 0) {
                return new JsonResponse($this->prepareResponse((string) $errors, Response::HTTP_NOT_ACCEPTABLE));
            }

            $handledStamp = $this->messageBus
                ->dispatch(new UpdateAlert($UuidAlertId, $alert->getType(), $alert->getValue()))
                ->last(HandledStamp::class)
                ->getResult();

            $response = $this->prepareResponse(
                'Alert updated successfully.',
                Response::HTTP_OK,
                $handledStamp->toArray()
            );
        } catch (InvalidArgumentException $exception) {
            $response = $this->prepareResponse((string) $exception, Response::HTTP_NOT_ACCEPTABLE);
        } catch (Exception $exception) {
            $response = $this->prepareResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }

    private function getRequestParams(Request $request): ?array
    {
        return json_decode($request->getContent(), true);
    }
}
