<?php

declare(strict_types=1);

namespace App\Service\Command;

use Exception;
use App\{Message\Command\DeleteAlert, Message\Query\Alert, Repository\AlertTypeRepository, Service\ServiceResponse};
use Symfony\Component\{HttpFoundation\JsonResponse,
    HttpFoundation\Response,
    Messenger\MessageBusInterface,
    Messenger\Stamp\HandledStamp,
    Uid\Uuid,
    Validator\Validator\ValidatorInterface};
use InvalidArgumentException;

class DeleteAlertService extends ServiceResponse
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private AlertTypeRepository $alertTypeRepository,
        private ValidatorInterface $validator
    ) { }

    public function delete(string $alertId): JsonResponse
    {
        try {
            $this->messageBus
                ->dispatch(new Alert(Uuid::fromString($alertId)))
                ->last(HandledStamp::class)
                ->getResult();

            $this->messageBus
                ->dispatch(new DeleteAlert(Uuid::fromString($alertId)))
                ->last(HandledStamp::class);

            $response = $this->prepareResponse(
                'Alert deleted successfully.',
                Response::HTTP_OK
            );
        } catch (InvalidArgumentException $exception) {
            $response = $this->prepareResponse((string) $exception, Response::HTTP_NOT_ACCEPTABLE);
        } catch (Exception $exception) {
            $response = $this->prepareResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }
}
