<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\{Entity\Alert,
    Message\Command\NewAlert,
    Message\Query\AlertType,
    Repository\AlertTypeRepository,
    Service\ServiceResponse};
use Exception;
use Symfony\Component\{HttpFoundation\JsonResponse,
    HttpFoundation\Request,
    HttpFoundation\Response,
    Messenger\MessageBusInterface,
    Messenger\Stamp\HandledStamp,
    Uid\Uuid,
    Validator\Validator\ValidatorInterface};
use InvalidArgumentException;

class NewAlertService extends ServiceResponse
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private AlertTypeRepository $alertTypeRepository,
        private ValidatorInterface $validator
    ) { }

    public function add(Request $request): JsonResponse
    {
        try {
            $requestParams = $this->getRequestParams($request);

            $alert = new Alert(
                Uuid::fromString($requestParams['userId']),
                $this->messageBus
                    ->dispatch(new AlertType((int) $requestParams['type']))
                    ->last(HandledStamp::class)
                    ->getResult(),
                (int) $requestParams['value'],
            );

            $errors = $this->validator->validate($alert);

            if (count($errors) > 0) {
                return new JsonResponse($this->prepareResponse((string) $errors, Response::HTTP_NOT_ACCEPTABLE));
            }

            $newAlert = $this->messageBus
                ->dispatch(new NewAlert($alert->getUserId(), $alert->getType(), $alert->getValue()))
                ->last(HandledStamp::class)
                ->getResult();

            $response = $this->prepareResponse(
                'Alert added successfully.',
                Response::HTTP_CREATED,
                $newAlert->toArray()
            );
        } catch (InvalidArgumentException $exception) {
            $response = $this->prepareResponse((string) $exception, Response::HTTP_NOT_ACCEPTABLE);
        } catch (Exception $exception) {
            $response = $this->prepareResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }

    private function getRequestParams(Request $request): array
    {
        return json_decode($request->getContent(), true);
    }
}
