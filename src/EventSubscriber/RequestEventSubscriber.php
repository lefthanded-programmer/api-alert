<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use Symfony\Component\{EventDispatcher\EventSubscriberInterface,
    HttpFoundation\JsonResponse,
    HttpKernel\Event\RequestEvent,
    HttpFoundation\Response,
    Uid\Uuid};

class RequestEventSubscriber implements EventSubscriberInterface
{
    private const ACCEPTABLE_REQUEST = 'json';

    public function onKernelRequest(RequestEvent $event)
    {
        $this->isJsonRequest($event);
        $this->validateAttributes($event);
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.request' => 'onKernelRequest',
        ];
    }

    private function isJsonRequest(RequestEvent $event): bool|Response
    {
        if ($event->getRequest()?->getContentType() !== self::ACCEPTABLE_REQUEST) {
            $event->setResponse(
                new Response('Unable to parse request. Use JSON requests.', Response::HTTP_BAD_REQUEST)
            );
        }

        return true;
    }

    private function validateAttributes(RequestEvent $event): bool|Response
    {
        if ($userId = $event->getRequest()?->attributes?->get('userId', false)) {
            !Uuid::isValid($event->getRequest()->attributes->get('userId'))
            ? $event->setResponse(new JsonResponse(
                [
                    'code' => Response::HTTP_BAD_REQUEST,
                    'message' => 'User ID isn\'t valid'
                ], Response::HTTP_BAD_REQUEST))
            : true;
        }

        if ($event->getRequest()?->attributes?->get('alertId', false)) {
            !Uuid::isValid($event->getRequest()->attributes->get('alertId'))
                ? $event->setResponse(new JsonResponse(
                [
                    'code' => Response::HTTP_BAD_REQUEST,
                    'message' => 'Alert ID isn\'t valid'
                ], Response::HTTP_BAD_REQUEST))
                : true;
        }

        return true;
    }
}
