<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\AlertRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidV4Generator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AlertRepository::class)
 */
class Alert
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidV4Generator::class)
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="uuid")
     *
     * @Assert\NotBlank
     * @Assert\Uuid
     */
    private Uuid $userId;

    /**
//     * //, cascade={"all"}
     * @ORM\ManyToOne(targetEntity=AlertType::class)
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\NotBlank
     */
    private AlertType $type;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\NotBlank
     * @Assert\Type("integer")
     */
    private int $value;

    public function __construct($userId, $type, $value)
    {
        $this->userId   = $userId;
        $this->type     = $type;
        $this->value    = $value;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }

    public function getType(): AlertType
    {
        return $this->type;
    }

    public function setType($type): void
    {
        $this->type = $type;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue($value): void
    {
        $this->value = $value;
    }

    public function toArray(): array
    {
        return [
            'id'    => $this->getId(),
            'type'  => $this->getType()->getName(),
            'value' => $this->getValue()
        ];
    }
}
