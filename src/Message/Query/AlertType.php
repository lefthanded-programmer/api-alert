<?php

declare(strict_types=1);

namespace App\Message\Query;

final class AlertType
{
    public function __construct(private int $alertTypeId) { }

    public function getAlertTypeId(): int
    {
        return $this->alertTypeId;
    }
}
