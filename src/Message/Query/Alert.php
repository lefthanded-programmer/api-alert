<?php

declare(strict_types=1);

namespace App\Message\Query;

use Symfony\Component\Uid\Uuid;

final class Alert
{
    public function __construct(private Uuid $alertId) { }

    public function getAlertId(): Uuid
    {
        return $this->alertId;
    }
}
