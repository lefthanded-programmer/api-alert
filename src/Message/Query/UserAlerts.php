<?php

declare(strict_types=1);

namespace App\Message\Query;

use Symfony\Component\Uid\Uuid;

final class UserAlerts
{
    public function __construct(private Uuid $userId, private int $limit, private int $offset) { }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }
}
