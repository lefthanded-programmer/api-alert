<?php

declare(strict_types=1);

namespace App\Message\Command;

use App\Entity\AlertType;
use Symfony\Component\Uid\Uuid;

final class UpdateAlert
{
    public function __construct(
        private Uuid $alertId,
        private AlertType $type,
        private int $value
    ) { }

    public function getAlertId(): Uuid
    {
        return $this->alertId;
    }

    public function getType(): AlertType
    {
        return $this->type;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
