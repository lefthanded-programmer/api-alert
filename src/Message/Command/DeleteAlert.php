<?php

declare(strict_types=1);

namespace App\Message\Command;

use Symfony\Component\Uid\Uuid;

final class DeleteAlert
{
    public function __construct(private Uuid $alertId) { }

    public function getAlertId(): Uuid
    {
        return $this->alertId;
    }
}
