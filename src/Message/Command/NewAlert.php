<?php

declare(strict_types=1);

namespace App\Message\Command;

use App\Entity\AlertType;
use Symfony\Component\Uid\Uuid;

final class NewAlert
{
    public function __construct(private Uuid $userId, private AlertType $type, private int $value) { }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }

    public function getType(): AlertType
    {
        return $this->type;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
