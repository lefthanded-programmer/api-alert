<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Alert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Alert|null find($id, $lockMode = null, $lockVersion = null)
 * @method Alert|null findOneBy(array $criteria, array $orderBy = null)
 * @method Alert[]    findAll()
 * @method Alert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlertRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Alert::class);
    }

    public function save(Alert $alert): Alert
    {
        try {
            $this->_em->persist($alert);
            $this->_em->flush();
        } catch (ORMException $exception) {

        }

        return $alert;
    }

    public function remove(Alert $alert): void
    {
        try {
            $this->_em->remove($alert);
            $this->_em->flush();
        } catch (ORMException $exception) {

        }
    }
}
