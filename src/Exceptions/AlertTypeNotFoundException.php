<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class AlertTypeNotFoundException extends Exception
{
    public function __construct(protected string $alertId)
    {
        parent::__construct(
            sprintf('NOT found alert type ID: %s.', $this->alertId),
            Response::HTTP_NOT_FOUND
        );
    }
}
