<?php

declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class AlertNotFoundException extends \Exception
{
    public function __construct(protected string $alertId)
    {
        parent::__construct(
            sprintf('NOT found alert ID: %s.', $this->alertId),
            Response::HTTP_NOT_FOUND
        );
    }
}
