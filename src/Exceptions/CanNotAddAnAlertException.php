<?php

declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class CanNotAddAnAlertException extends \Exception
{
    public function __construct(protected $message)
    {
        parent::__construct(
            sprintf('Can\'t add an alert: %s because: ', $this->message),
            Response::HTTP_BAD_REQUEST
        );
    }
}
