<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class UserHasNotAnAlerts extends Exception
{
    public function __construct(protected string $userId)
    {
        parent::__construct(
            sprintf('NOT found alerts for User ID: %s.', $this->userId),
            Response::HTTP_NO_CONTENT
        );
    }
}