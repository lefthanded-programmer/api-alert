<?php

declare(strict_types=1);

namespace App\Controller\v2;

use Symfony\Component\HttpFoundation\{Request, Response};
use Symfony\Component\Routing\Annotation\Route;

class AlertsController
{
    #[Route('alerts', name: 'alerts_1_1_list', methods: [Request::METHOD_GET])]
    public function listOfAlerts(): Response
    {
        return new Response(
            'alerts_1_1_list'
        );
    }

    #[Route('alerts/{id}', name: 'alerts_1_1_alert', methods: [Request::METHOD_GET])]
    public function getAlert($id): Response
    {

        return new Response(
            'alerts_1_1_alert'
        );
    }

    #[Route('alerts', name: 'alerts_1_1_new', methods: [Request::METHOD_POST])]
    public function newAlert($id): Response
    {
        return new Response(
            'alerts_1_1_new'
        );
    }

    #[Route('alerts/{id}', name: 'alerts_1_1_update', methods: ['PUT'])]
    public function updateAlert($id): Response
    {
        return new Response(
            'alerts_1_1_update'
        );
    }

    #[Route('alerts/{id}', name: 'alerts_1_1_partial_update', methods: ['PATCH'])]
    public function partialUpdateAlert($id): Response
    {
        return new Response(
            'alerts_1_1_partial_update'
        );
    }

    #[Route('alerts/{id}', name: 'alerts_1_1_partial_delete', methods: ['DELETE'])]
    public function deleteAlert($id): Response
    {
        return new Response(
            'alerts_1_1_partial_delete'
        );
    }
}
