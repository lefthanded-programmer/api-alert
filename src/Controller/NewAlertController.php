<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Command\NewAlertService;
use Symfony\{Component\HttpFoundation\JsonResponse,
    Component\HttpFoundation\Request,
    Component\Routing\Annotation\Route};

#[Route('alerts', name: 'alerts_1_0_new', methods: [Request::METHOD_POST])]
class NewAlertController
{
    public function __construct(
        private NewAlertService $service
    ) { }

    public function __invoke(Request $request): JsonResponse
    {
        return $this->service->add($request);
    }
}
