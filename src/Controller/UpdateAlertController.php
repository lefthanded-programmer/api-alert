<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Command\UpdateAlertService;
use Symfony\{Component\HttpFoundation\JsonResponse,
    Component\HttpFoundation\Request,
    Component\Routing\Annotation\Route};

#[Route('alerts/{alertId}', name: 'alerts_1_0_update', methods: [Request::METHOD_PUT])]
class UpdateAlertController
{
    public function __construct(
        private UpdateAlertService $service
    ) { }

    public function __invoke(string $alertId, Request $request): JsonResponse
    {
        return $this->service->update($alertId, $request);
    }
}
