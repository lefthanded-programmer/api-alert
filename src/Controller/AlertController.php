<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Query\AlertService;
use Symfony\Component\{HttpFoundation\JsonResponse, HttpFoundation\Request, Routing\Annotation\Route};

#[Route('alerts/{alertId}', name: 'alerts_1_alert', methods: [Request::METHOD_GET])]
class AlertController
{
    public function __construct(private AlertService $service) { }

    public function __invoke(string $alertId): JsonResponse
    {
        return $this->service->get($alertId);
    }
}
