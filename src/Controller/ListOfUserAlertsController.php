<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Query\ListOfUserAlertsService;
use Symfony\{Component\HttpFoundation\JsonResponse,
    Component\HttpFoundation\Request,
    Component\Routing\Annotation\Route};

#[Route('alerts/{userId}/all/{limit}/{offset}', name: 'alerts_1_0_list', methods: [Request::METHOD_GET])]
class ListOfUserAlertsController
{
    private const LIMIT = 10;
    private const OFFSET = 0;

    public function __construct(
        private ListOfUserAlertsService $service
    ) { }

    public function __invoke(string $userId, int $limit = self::LIMIT, int $offset = self::OFFSET): JsonResponse
    {
        return $this->service->get($userId, $limit, $offset);
    }
}
