<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Command\DeleteAlertService;
use Symfony\{Component\HttpFoundation\JsonResponse,
    Component\HttpFoundation\Request,
    Component\Routing\Annotation\Route};

#[Route('alerts/{alertId}', name: 'alerts_1_0_partial_delete', methods: [Request::METHOD_DELETE])]
class DeleteAlertController
{
    public function __construct(
        private DeleteAlertService $service
    ) { }

    public function __invoke(string $alertId): JsonResponse
    {
        return $this->service->delete($alertId);
    }
}
