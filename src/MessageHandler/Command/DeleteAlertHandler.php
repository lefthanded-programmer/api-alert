<?php

declare(strict_types=1);

namespace App\MessageHandler\Command;

use App\Exceptions\AlertNotFoundException;
use App\Exceptions\CanNotDeleteAnAlertException;
use App\Message\Command\DeleteAlert;
use App\Repository\AlertRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteAlertHandler implements MessageHandlerInterface
{
    public function __construct(private AlertRepository $alertRepository) { }

    public function __invoke(DeleteAlert $newAlert): bool|CanNotDeleteAnAlertException
    {
        try {
            if (is_null($alert = $this->alertRepository->find($newAlert->getAlertId())))
                throw new AlertNotFoundException((string) $newAlert->getAlertId());

            $this->alertRepository->remove($alert);
        } catch (\Exception $exception) {
            throw new CanNotDeleteAnAlertException($exception);
        }

        return true;
    }
}
