<?php

declare(strict_types=1);

namespace App\MessageHandler\Command;

use App\{Entity\Alert,
    Exceptions\CanNotUpdateAnAlertException,
    Message\Command\UpdateAlert,
    Repository\AlertRepository};
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdateAlertHandler implements MessageHandlerInterface
{
    public function __construct(private AlertRepository $alertRepository) { }

    public function __invoke(UpdateAlert $queryAlert): Alert|CanNotUpdateAnAlertException
    {
        try {
            $alert = $this->alertRepository->find($queryAlert->getAlertId());

            $alert->setValue($queryAlert->getValue());
            $alert->setType($queryAlert->getType());

            $this->alertRepository->save($alert);
        } catch (\Exception $exception) {
            throw new CanNotUpdateAnAlertException($exception);
        }

        return $alert;
    }
}
