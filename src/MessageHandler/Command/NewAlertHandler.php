<?php

declare(strict_types=1);

namespace App\MessageHandler\Command;

use App\{Entity\Alert, Exceptions\CanNotAddAnAlertException, Message\Command\NewAlert, Repository\AlertRepository};
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class NewAlertHandler implements MessageHandlerInterface
{
    public function __construct(private AlertRepository $alertRepository) { }

    public function __invoke(NewAlert $newAlert): Alert|CanNotAddAnAlertException
    {
        try {
            $alert = new Alert($newAlert->getUserId(), $newAlert->getType(), $newAlert->getValue());

            $this->alertRepository->save($alert);
        } catch (\Exception $exception) {
            throw new CanNotAddAnAlertException($exception);
        }

        return $alert;
    }
}
