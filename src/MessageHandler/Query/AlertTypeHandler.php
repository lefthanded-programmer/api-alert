<?php

declare(strict_types=1);

namespace App\MessageHandler\Query;

use App\{Entity\AlertType,
    Exceptions\AlertTypeNotFoundException,
    Message\Query\AlertType as AlertTypeQuery,
    Repository\AlertTypeRepository};
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AlertTypeHandler implements MessageHandlerInterface
{
    public function __construct(private AlertTypeRepository $alertTypeRepository) { }

    public function __invoke(AlertTypeQuery $alert): AlertType|AlertTypeNotFoundException
    {
        $response = $this->alertTypeRepository->find($alert->getAlertTypeId());

        if (is_null($response)) throw new AlertTypeNotFoundException((string) $alert->getAlertTypeId());

        return $response;
    }
}
