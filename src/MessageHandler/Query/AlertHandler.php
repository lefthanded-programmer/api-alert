<?php

declare(strict_types=1);

namespace App\MessageHandler\Query;

use App\{Entity\Alert,
    Exceptions\AlertNotFoundException,
    Message\Query\Alert as AlertQuery,
    Repository\AlertRepository};
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AlertHandler implements MessageHandlerInterface
{
    public function __construct(private AlertRepository $alertRepository) { }

    public function __invoke(AlertQuery $alert): Alert|AlertNotFoundException
    {
        $response = $this->alertRepository->find($alert->getAlertId());

        if (is_null($response)) throw new AlertNotFoundException((string) $alert->getAlertId());

        return $response;
    }
}
