<?php

declare(strict_types=1);

namespace App\MessageHandler\Query;

use App\{Exceptions\UserHasNotAnAlerts,
    Message\Query\UserAlerts,
    Repository\AlertRepository,
    Repository\AlertTypeRepository};
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UserAlertsHandler implements MessageHandlerInterface
{
    public function __construct(
        private AlertRepository $alertRepository,
        private AlertTypeRepository $alertKindRepository
    ) { }

    public function __invoke(UserAlerts $userAlerts): array|UserHasNotAnAlerts
    {
        $alerts = $this->alertRepository->findBy(
            [
                'userId' => $userAlerts->getUserId(),
            ],
            [],
            $userAlerts->getLimit(),
            $userAlerts->getOffset()
        );

        return $this->parseResponse($alerts);
    }

    private function parseResponse(array $alerts): array
    {
        $parsedAlerts = [];

        foreach ($alerts as $alert) {
            $parsedAlerts[] = $alert->toArray();
        }

        return $parsedAlerts;
    }
}
