<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Uid\Uuid;

class NewAlertControllerTest extends WebTestCase
{
    private const TYPE_CURRENCY   = 'currency';
    private const TYPE_PERCENTAGE = 'percentage';

    public const TYPE = [
        1 => self::TYPE_CURRENCY,
        2 => self::TYPE_PERCENTAGE
    ];

    public function testNewAlert($userId = null): array
    {
        $userId = $userId ?? Uuid::v4();
        $type   = rand(1, 2);
        $value  = rand(1, 9999);

        $content = [
            'userId' => $userId,
            'type'   => $type,
            'value'  => $value
        ];

        $client = static::createClient();
        $client->request(
            'POST',
            '/v1/alerts',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($content)
        );

        $alert = (array) json_decode($client->getResponse()->getContent())->body;

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(201, (string) $client->getResponse()->getStatusCode());
        $this->assertEquals(self::TYPE[$type], $alert['type']);
        $this->assertEquals($value, $alert['value']);

        static::tearDown();

        return $alert;
    }

    public function testFailureUserId()
    {
        $content = [
            'userId' => '5283f2f3-a6e7-4f50-9216',
            'type'   => rand(1, 2),
            'value'  => rand(1, 9999)
        ];

        $client = static::createClient();
        $client->request(
            'POST',
            '/v1/alerts',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($content)
        );

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(406, (string) $client->getResponse()->getStatusCode());

        static::tearDown();
    }

    public function testFailureAlertType()
    {
        $content = [
            'userId' => Uuid::v4(),
            'type'   => 3,
            'value'  => rand(1, 9999)
        ];

        $client = static::createClient();
        $client->request(
            'POST',
            '/v1/alerts',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($content)
        );

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(404, (string) $client->getResponse()->getStatusCode());

        static::tearDown();
    }
}
