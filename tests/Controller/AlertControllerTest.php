<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Uid\Uuid;

class AlertControllerTest extends WebTestCase
{
    private array $alert;

    protected function setUp(): void
    {
        $this->alert = (new NewAlertControllerTest())->testNewAlert();
    }

    public function testGetAlert(): array
    {
        $client = static::createClient();
        $client->request(
            'GET',
            sprintf('/v1/alerts/%s', $this->alert['id']),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
        );

        $alert = (array) json_decode($client->getResponse()->getContent())?->body;

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(200, (string) $client->getResponse()->getStatusCode());
        $this->assertEquals($this->alert['type'], $alert['type']);
        $this->assertEquals($this->alert['value'], $alert['value']);

        static::tearDown();

        return $alert;
    }

    public function testFailureAlertId()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            sprintf('/v1/alerts/%s', (string) Uuid::v4()),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
        );

        $alert = (array) json_decode($client->getResponse()->getContent())->body;

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(404, (string) $client->getResponse()->getStatusCode());
        $this->assertCount(0, $alert);

        static::tearDown();
    }

    public function testFailureUuid()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            sprintf('/v1/alerts/%s', '5283f2f3-a6e7-4f50-9216'),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
        );

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(400, (string) $client->getResponse()->getStatusCode());

        static::tearDown();
    }
}
