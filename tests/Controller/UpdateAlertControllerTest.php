<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Uid\Uuid;

class UpdateAlertControllerTest extends WebTestCase
{
    public function testUpdateAlert()
    {
        $newAlertController = new NewAlertControllerTest();
        $alert = $newAlertController->testNewAlert();

        $type   = rand(1, 2);
        $value  = rand(1, 9999);

        $content = [
            'type'  => $type,
            'value' => $value
        ];

        $client = static::createClient();
        $client->request(
            'PUT',
            sprintf('/v1/alerts/%s', $alert['id']),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($content)
        );

        $alert = (array) json_decode($client->getResponse()->getContent())->body;

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(200, (string) $client->getResponse()->getStatusCode());
        $this->assertEquals($newAlertController::TYPE[$type], $alert['type']);
        $this->assertEquals($value, $alert['value']);

        static::tearDown();
    }

    public function testFailureUserId()
    {
        $content = [
            'userId' => '5283f2f3-a6e7-4f50-9216',
            'type'   => rand(1, 2),
            'value'  => rand(1, 9999)
        ];

        $client = static::createClient();
        $client->request(
            'POST',
            '/v1/alerts',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($content)
        );

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(406, (string) $client->getResponse()->getStatusCode());

        static::tearDown();
    }

    public function testFailureAlertType()
    {
        $content = [
            'userId' => Uuid::v4(),
            'type'   => 3,
            'value'  => rand(1, 9999)
        ];

        $client = static::createClient();
        $client->request(
            'POST',
            '/v1/alerts',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($content)
        );

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(404, (string) $client->getResponse()->getStatusCode());

        static::tearDown();
    }
}
