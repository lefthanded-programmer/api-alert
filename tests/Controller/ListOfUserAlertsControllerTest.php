<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Uid\Uuid;

class ListOfUserAlertsControllerTest extends WebTestCase
{
    public function testGetUserAlerts()
    {
        $userId = Uuid::v4();

        $userAlertsCount = 5;
        for ($i = 0; $i < $userAlertsCount; $i++) {
            (new NewAlertControllerTest())->testNewAlert($userId);
        }

        $client = static::createClient();
        $client->request(
            'GET',
            sprintf('/v1/alerts/%s/all', $userId),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $userAlerts = (array) json_decode($client->getResponse()->getContent())->body;

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(200, (string) $client->getResponse()->getStatusCode());
        $this->assertCount($userAlertsCount, $userAlerts);

        static::tearDown();
    }

    public function testGetUserAlertsWhenUserHasNotAnyAlert()
    {
        $userId = Uuid::v4();

        $client = static::createClient();
        $client->request(
            'GET',
            sprintf('/v1/alerts/%s/all', $userId),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $userAlerts = (array) json_decode($client->getResponse()->getContent())?->body;

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(200, (string) $client->getResponse()->getStatusCode());
        $this->assertCount(0, $userAlerts);

        static::tearDown();
    }

    public function testFailureUuid()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            sprintf('/v1/alerts/%s/all', '5283f2f3-a6e7-4f50-9216'),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
        );

        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(400, (string) $client->getResponse()->getStatusCode());

        static::tearDown();
    }
}
