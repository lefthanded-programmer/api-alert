<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Service\ServiceResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class ServiceResponseTest extends TestCase
{
    public function testPrepareResponse()
    {
        $response = (new ServiceResponse())->prepareResponse(
            'Message to test',
            Response::HTTP_OK,
            [json_encode([0,1,2,3])]
        );

        $this->assertIsArray($response);
    }
}