<?php

declare(strict_types=1);

namespace App\Tests\Service\Query;

use App\Service\Query\AlertService;
use App\Tests\Controller\NewAlertControllerTest;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Uid\Uuid;

class AlertServiceTest extends WebTestCase
{
    private array $alert;

    protected function setUp(): void
    {
        $this->alert = (new NewAlertControllerTest())->testNewAlert();
    }

    public function testGet()
    {
        $client = static::createClient();
        $messageBus = $client->getContainer()->get('messenger.default_bus');

        $alertService = new AlertService($messageBus);

        $response = $alertService->get($this->alert['id']);

        $alert = json_decode($response->getContent(), true);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('code', $alert);
        $this->assertArrayHasKey('message', $alert);
        $this->assertArrayHasKey('body', $alert);

        static::tearDown();
    }

    public function testFailureAlertId()
    {
        $client = static::createClient();
        $messageBus = $client->getContainer()->get('messenger.default_bus');

        $alertService = new AlertService($messageBus);

        $response = $alertService->get((string) Uuid::v4());

        $alert = json_decode($response->getContent(), true);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertCount(0, $alert['body']);

        static::tearDown();
    }

    public function testFailureUuid()
    {
        $client = static::createClient();
        $messageBus = $client->getContainer()->get('messenger.default_bus');

        $alertService = new AlertService($messageBus);

        $response = $alertService->get('5283f2f3-a6e7-4f50-9216');

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(406, (string) $response->getStatusCode());

        static::tearDown();
    }
}